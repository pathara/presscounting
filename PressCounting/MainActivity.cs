﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Media;
using Java.Lang;
using System.Threading;

namespace PressCounting
{
	[Activity(Label = "PressCounting", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		int count = 1;

		// Encoding
		// Ref : https://developer.android.com/reference/android/media/AudioFormat.html#ENCODING_PCM_16BIT
		int ENCODING_PCM_16BIT = 2;
		int ENCODING_PCM_8BIT = 3;
		int CHANNEL_IN_MONO = 16;
		int CHANNEL_IN_STEREO = 12;
		int ERROR_BAD_VALUE = -2;

		// Ref : https://developer.android.com/reference/android/media/AudioRecord.html#STATE_INITIALIZED
		int STATE_INITIALIZED = 1;

		// Ref : https://developer.android.com/reference/android/media/MediaRecorder.AudioSource.html#MIC
		int MIC = 1;

		private static int[] mSampleRates = new int[] { 8000, 11025, 22050, 44100 };
		private bool mShouldContinue = true; // Indicates if recording / playback should stop
		private int SAMPLING_RATE = 44100;
		private int bufferSize;
		private AudioRecord record;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button>(Resource.Id.myButton);

			Android.OS.Process.SetThreadPriority(Android.OS.ThreadPriority.Audio);

			// buffer size in bytes
			bufferSize = AudioRecord.GetMinBufferSize(
				SAMPLING_RATE,
				(ChannelIn)2,
				(Encoding)ENCODING_PCM_16BIT);

			//if (bufferSize == AudioRecord. .ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE)
			//{
			//	bufferSize = SAMPLE_RATE * 2;
			//}



			//record = new AudioRecord(
			//	(AudioSource)MIC,
			//	SAMPLING_RATE,
			//	(ChannelIn)CHANNEL_IN_MONO,
			//	(Encoding)ENCODING_PCM_16BIT,
			//	bufferSize);

			record = findAudioRecord();

			if (record.State != (State)STATE_INITIALIZED)
			{
				Toast.MakeText(ApplicationContext, "Audio Record can't initialize!", ToastLength.Long).Show();
				return;
			}

			button.Click += delegate { 
				button.Text = $"{count++} clicks!";
				//receive();
				recordAudio();
			};
		}

		public void receive()
		{
			int minBufferSize = AudioRecord.GetMinBufferSize(
				SAMPLING_RATE, 
				(ChannelIn)2, 
				(Encoding)ENCODING_PCM_16BIT);
			AudioRecord ar = new AudioRecord(
				(AudioSource)MIC,
				SAMPLING_RATE, 
				(ChannelIn)CHANNEL_IN_MONO,
				(Encoding)ENCODING_PCM_16BIT, 
				minBufferSize);
			ar.StartRecording();
		}

		void recordAudio()
		{
			System.Threading.Thread recordThread = new System.Threading.Thread(new ThreadStart(Run));
			recordThread.Start();
		}

		public void Run()
		{
			short[] audioBuffer = new short[bufferSize / 2];


			record.StartRecording();

			Toast.MakeText(ApplicationContext, "Start recording", ToastLength.Long).Show();

			long shortsRead = 0;
			while (mShouldContinue)
			{
				int numberOfShort = record.Read(audioBuffer, 0, audioBuffer.Length);
				shortsRead += numberOfShort;

				Toast.MakeText(
					ApplicationContext,
					("Recording stopped. Samples read: " + shortsRead),
					ToastLength.Long).Show();

				// Do something with the audioBuffer
			}

			record.Stop();
			record.Release();

			Toast.MakeText(
				ApplicationContext, 
				("Recording stopped. Samples read: "+ shortsRead), 
				ToastLength.Long).Show();
		}

		public AudioRecord findAudioRecord()
		{
			for (int rate=0;rate < mSampleRates.Length; rate++)
			{
				int[] mAudioFormat = new int[] { ENCODING_PCM_8BIT, ENCODING_PCM_16BIT };
				for (int audioFormat = 0; audioFormat<mAudioFormat.Length; audioFormat++)
				{
					int[] mChannelConfig = new int[] { CHANNEL_IN_MONO, CHANNEL_IN_STEREO };
					for (int channelConfig = 0; channelConfig < mChannelConfig.Length; channelConfig++)
					{
						try
						{
							Toast.MakeText(
								ApplicationContext, 
									"Attempting rate " + mSampleRates[rate] + "Hz, bits: " + mAudioFormat[audioFormat] + 
									", channel: " + mChannelConfig[channelConfig],
								ToastLength.Long).Show();
							
							int bufferSize = AudioRecord.GetMinBufferSize(
								mSampleRates[rate], 
								(ChannelIn)mChannelConfig[channelConfig], 
								(Encoding)mAudioFormat[audioFormat]);

							if (bufferSize != ERROR_BAD_VALUE)
							{
								// check if we can instantiate and have a success
								AudioRecord recorder = new AudioRecord(
									0, 
									mSampleRates[rate], 
									(ChannelIn)mChannelConfig[channelConfig], 
									(Encoding)mAudioFormat[audioFormat], 
									bufferSize);

								if (recorder.State == (State)STATE_INITIALIZED)
									return recorder;
							}
						}
						catch (Exception e)
						{
							Toast.MakeText(
								ApplicationContext, 
								rate + "Exception, keep trying.",
								ToastLength.Long).Show();
						}
					}
				}
			}
			return null;
		}
	}
}

